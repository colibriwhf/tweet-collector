# README #

Questo repository contiene uno script python per utilizzare le API di twittere e scaricare dei tweet tematici in file JSON sul proprio filesystem locale. 

* Per utilizzarlo è necessario avere le credenziali da "twitter developer" (https://developer.twitter.com) e inserirle all'interno dello script valorizzando le costanti in testa. 
* Attualmente lo script è impostato per "sintonizzarsi" sui tweet a tema "covid". E' ovviamente possibile modificare il parametro relativo.
* Un file viene scritto solamente dopo aver raccolto 10000 tweet sul tema. Anche questo parametro è configurabile all'interno dello script. 
* La struttura dei JSON salvati è la seguente: 
    {
        "author": "USERNAME",
        "text": "TEXT",
        "created_at": "YYYY-MM-DD HH:mm:ss",
        "source": "",
        "Location": {
            "place_name": "CITY",
            "place_full_name": "CITY, STATE",
            "place_country_code": "STATE CODE",
            "place_coordinates": "[[[LAT, LON], [LAT, LON], [LAT, LON], [LAT, LON]]]"
        }
    },