import tweepy
import sys
import json
import os

CONSUMER_KEY = ""
CONSUMER_SECRET = ""
ACCESS_TOKEN = ""
ACCESS_TOKEN_SECRET = ""

TOPIC = "covid"
SAVE_PATH = "/home/"+os.environ['USER']+"/nifi-docker-volume/input-datasets/twitter-flow"
TWEET_THRESHOLD = 1000


auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

api = tweepy.API(
    auth, 
    wait_on_rate_limit=True,
    wait_on_rate_limit_notify=True
)


class TweetListener(tweepy.StreamListener):

    def __init__(self, output_stream=sys.stdout):
        super(TweetListener,self).__init__()
        self.output_stream = output_stream
        self.list_of_tweets = []
        self.counter = 0
        self.num_file = 0

    def on_status(self, status):    
        # print("Author: {},\n Text: {},\n Datetime: {},\n Source: {},\n Place: {}".format(
        #     status.author.screen_name, 
        #     status.full_text if hasattr(status, 'full_text') else status.text, 
        #     str(status.created_at),
        #     status.source,
        #     status.place
        # ), file=self.output_stream)
        
        if self.counter%10==0: 
            print('Fetched: ' + str(self.counter) + ' tweets')

        self.counter = self.counter + 1
        self.list_of_tweets.append({
            'author': status.author.screen_name,
            'text': status.full_text if hasattr(status, 'full_text') else status.text,
            'created_at': str(status.created_at),
            'source': status.source,
            'Location': {'place_name': status.place.name, 'place_full_name': status.place.full_name, 'place_country_code': status.place.country_code, 'place_coordinates': str(status.place.bounding_box.coordinates)} if status.place is not None else None
        })

        if self.counter == TWEET_THRESHOLD:
            file_name = SAVE_PATH+"/data-"+str(self.num_file)+".json" 
            with open(file_name, 'w', encoding='utf-8') as f:
                json.dump(self.list_of_tweets, f, ensure_ascii=False, indent=4)
            self.counter = 0
            self.num_file = self.num_file + 1
            self.list_of_tweets = []

    def on_error(self, status_code):
        print(status_code)
        return False

tweet_listener = TweetListener()
tweet_stream = tweepy.Stream(auth=api.auth, listener=tweet_listener,tweet_mode='extended')


try:
    print('Start streaming.')
    #filter(locations=[103.60998,1.25752,104.03295,1.44973], track=['twitter'])
    tweet_stream.filter(track=[TOPIC])
except KeyboardInterrupt as e :
    print("Stopped.")
finally:
    print('Done.')
    tweet_stream.disconnect()
